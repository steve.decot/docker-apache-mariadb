# deployement script bash

DOMAINTLD=try.anoskar.be # 000-default.conf / apache2.conf / docker-compose.yml
ADMIN_SITE_EMAIL=anoskar@anoskar.be # 000-default.conf

#--------------------
# docker-compose.yml
# -------------------
NETWORK_APACHE=web
NETWORK_MARIADB_APACHE=db
ROUTER_TRAEFIK_APACHE=$(echo $DOMAINTLD | sed "s/\./_/g")
MARIADB_ROOT_PASSWORD=imroot
MARIADB_USER=billy
MARIADB_PASSWORD=kimba
MARIADB_DATABASE=apachedb


sed -i "s/\${NETWORK_APACHE}/$NETWORK_APACHE/g" docker-compose.yml
sed -i "s/\${NETWORK_MARIADB_APACHE}/$NETWORK_MARIADB_APACHE/g" docker-compose.yml
sed -i "s/\${ROUTER_TRAEFIK_APACHE}/$ROUTER_TRAEFIK_APACHE/g" docker-compose.yml
sed -i "s/\${MARIADB_ROOT_PASSWORD}/$MARIADB_ROOT_PASSWORD/g" docker-compose.yml
sed -i "s/\${MARIADB_USER}/$MARIADB_USER/g" docker-compose.yml
sed -i "s/\${MARIADB_PASSWORD}/$MARIADB_PASSWORD/g" docker-compose.yml
sed -i "s/\${MARIADB_DATABASE}/$MARIADB_DATABASE/g" docker-compose.yml 
sed -i "s/\${DOMAINTLD}/$DOMAINTLD/g" docker-compose.yml

sed -i "s/\${DOMAINTLD}/$DOMAINTLD/g" ./svc_www_apache/000-default.conf 
sed -i "s/\${DOMAINTLD}/$DOMAINTLD/g" ./svc_www_apache/apache2.conf 
sed -i "s/\${ADMIN_SITE_EMAIL}/$ADMIN_SITE_EMAIL/g" ./svc_www_apache/000-default.conf 

mkdir -p ./volumes/www_$DOMAINTLD/$DOMAINTLD/
echo "Bienvenue sur $DOMAINTLD" > ./volumes/www_$DOMAINTLD/$DOMAINTLD/index.php
docker-compose up --d --build && docker-compose logs -f 