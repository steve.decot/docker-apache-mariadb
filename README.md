# docker-apache-mariadb
## 31-05-2022
## Description
Stack apache + PHP couplée à mariadb "prête à l'emploi"  
Le tag du container PHP est ```php:7.4-apache-buster```
Après avoir complété les variables d'environement dans le script bash ```deploy.sh``` il suffira de le lancer afin qu'il crée votre instance. 

Une fois celle-ci crée, déposez vos fichiers dans le répertoire suivant ```./volumes/www_${DOMAINTLD}``` 

## Contenu du repo
* Le répertoire ```svc_www_apache```  qui contient de quoi build un apache avec php.
* Le ```deploy.sh```  qui, une fois les variables complétées vous déploiera votre stack fonctionnelle 
* Le ``` docker-compose.yml``` 

### ```svc_www_apache``` 
Contient 

* Le Dockerfile
* 000-default.conf
* apache2.conf


#### Dockerfile de PHP / apache
Il se trouve dans 

Ce dernier se base sur ```php:7.4-apache-buster``` dans lequel les packages suivants sont ajoutés

* vim 
* libfreetype6-dev 
* libjpeg62-turbo-dev 
* libpng-dev 
* libzip-dev 

Et les extensions PHP Core suivantes :

* gd
* mysqli
* zip

Les fichiers apache2.conf et 000-default.conf seront copiés dans l'image qui sera buildée. 

### deploy.sh

Ce script bash va, une fois ses variables affectées selon vos besoins, déployer la stack. 

Voici les variables

* DOMAINTLD=try.anoskar.be # 000-default.conf / apache2.conf / docker-compose.yml
* ADMIN_SITE_EMAIL=anoskar@anoskar.be # 000-default.conf
* NETWORK_APACHE=web
* NETWORK_MARIADB_APACHE=db
* ROUTER_TRAEFIK_APACHE=$(echo $DOMAINTLD | sed "s/\./_/g")
* MARIADB_ROOT_PASSWORD=imroot
* MARIADB_USER=billy
* MARIADB_PASSWORD=kimba
* MARIADB_DATABASE=apachedb


### docker-compose.yml
Le compose lancera la stack et est configuré via les labels pour fonctionner avec Traefik. Le repo est ici https://gitlab.com/steve.decot/docker-traefik

## Soon 
* Brancher le log
* Créer le rôle ansible pour le déploiement 
* Simplifier les commandes sed dans le ```deploy.sh``` (foreach)
